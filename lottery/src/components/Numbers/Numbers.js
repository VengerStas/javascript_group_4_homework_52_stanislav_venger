import React, {Component} from 'react';
import './Numbers.css';

class Numbers extends Component {
    state = {
        numberArray: []
    };

    getNumberOnClick = () => {
        const number = [];
        for (let i = 0; i < 5;){
            let random = Math.floor(Math.random() * 32 + 5);
            if (!number.includes(random)) {
                number.push(random);
                i++;
            }
        }
        number.sort((a, b) => a - b);
        this.setState({numberArray: number});
    };

    render() {
        return (
            <div className="numbers">
                <button onClick={this.getNumberOnClick} className="btn">New numbers</button>
                <div className="container">
                    {this.state.numberArray.map((number, index) => <p key={index} className="number">{number}</p>)}
                </div>
            </div>
        )
    }
}

export default Numbers;