import React, { Component } from 'react';
import  Numbers from './components/Numbers/Numbers';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Numbers />
      </div>
    );
  }
}

export default App;
